import React from "react";
import { FormGroup, FormControl, Row, Alert, Grid, Col } from "react-bootstrap";
import { Redirect } from "react-router-dom";

export default class Form extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      value: "",
      canSend: false
    };
  }

  handleChange = e => {
    if (e.target.value === this.props.grant) {
      this.setState({ canSend: true });
    }
    this.setState({ value: e.target.value });
  };

  render() {
    console.log(this.state.canSend);
    console.log(this.state.value);
    return (
      <Grid>
        <Col md={4}>
          <Row>
            <form>
              <FormGroup controlId="formBasicText">
                <FormControl
                  type="text"
                  value={this.state.value}
                  placeholder={this.props.placeholder}
                  onChange={this.handleChange}
                />
                <FormControl.Feedback />
                {this.state.value === "" ? null : this.state.canSend ? (
                  <Redirect to="/map" />
                ) : (
                  <Error />
                )}
              </FormGroup>
            </form>
          </Row>
        </Col>
      </Grid>
    );
  }
}

const Error = () => (
  <Row style={{ paddingTop: "15px" }}>
    <Alert bsStyle="danger">
      Número de <strong>Pin Incorrecto</strong>
    </Alert>
  </Row>
);
