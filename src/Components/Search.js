import React from "react";
import { Link } from "react-router-dom";
import {
  FormGroup,
  FormControl,
  Grid,
  Col,
  Row,
  ButtonToolbar,
  Dropdown,
  MenuItem,
  Well,
  Alert,
  ToggleButtonGroup,
  ToggleButton
} from "react-bootstrap";

export default class Search extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      value: "",
      currentActivos: props.activos,
      open: false,
      selected: "",
      format: ""
    };
  }

  componentDidMount() {
    this.setState({ loading: false });
  }

  handleChange = e => {
    const input = e.target.value;
    const currentActivos = this.props.activos.filter(_ => {
      const regex = RegExp(input, "i");
      return regex.test(_.name);
    });
    if(currentActivos.length === 0) {
      currentActivos[0] = { name:"Activo no disponible"};
    } 
    this.setState({ value: input, currentActivos, open: true });
    if (input === "") {
      this.setState({ open: false, selected: "" });
    }    
  };
  handleClick = name => {
    this.setState({ selected: name, open: false });
  };

  render() {
    const mapedActivos = this.state.currentActivos.map((_, key) => {
      return (
        <MenuItem key={key} eventKey={_.name} onSelect={this.handleClick}>
          {_.name}
        </MenuItem>
      );
    });
    return (
      <Grid
        style={{
          paddingTop: "35px"
        }}
      >
        <Col md={4}>
          <Row>
            <form>
              <FormGroup controlId="formBasicText">
                <FormControl
                  type="text"
                  value={this.state.value}
                  placeholder={"Busca tu activo"}
                  onChange={this.handleChange}
                />
              </FormGroup>
            </form>
          </Row>

          <Row>
            <ButtonToolbar>
              <Dropdown
                open={this.state.open}
                id="super-id"
                defaultOpen="false"
              >
                <Dropdown.Toggle />
                <Dropdown.Menu>{mapedActivos}</Dropdown.Menu>
              </Dropdown>
            </ButtonToolbar>
          </Row>
          <Row style={{ paddingTop: "15px" }}>
            {this.state.selected === "" ? null : (
              <div>
                <Well>{this.state.selected}</Well>
                <Selected />
              </div>
            )}
          </Row>
          <Row>{this.state.format}</Row>
        </Col>
      </Grid>
    );
  }
}

class Selected extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      value: 0
    };
  }

  handleChange = e => {
    this.setState({ value: e });
  };

  render() {
    return (
      <div>
        <ButtonToolbar style={{ alignSelf: "center", display: "flex" }}>
          <ToggleButtonGroup
            value={this.state.value}
            onChange={this.handleChange}
            type="radio"
            name="options"
          >
            <ToggleButton value={1}>Barras</ToggleButton>
            <ToggleButton value={2}>Tabletas</ToggleButton>
            <ToggleButton value={3}>Unguento</ToggleButton>
          </ToggleButtonGroup>
        </ButtonToolbar>
        {this.state.value > 0 ? (
          <div>
            <Alert bsStyle="warning" style={{ marginTop: "25px" }}>
              <strong>Solicita tu Pin</strong> para obtener la direccion
              llamando al 0212-7630923
            </Alert>
            <Link to="/pin">Siguiente</Link>
          </div>
        ) : null}
      </div>
    );
  }
}
