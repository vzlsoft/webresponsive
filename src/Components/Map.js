import React from "react";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import Loading from "./Loading";

export class MapContainer extends React.Component {
  render() {
    return (
      <Map
        style={{
          margin: "auto",

          display: "flex",
          alignSelf: "center",

          overflow: "hidden",
          padding: "10px",
          maxWidth: "990px",
          height: "90%",
          width: "90%",
          position: "relative"
        }}
        google={this.props.google}
        zoom={17}
        initialCenter={{
          lat: 10.490106,
          lng: -66.875577
        }}
      >
        <Marker
          onClick={this.onMarkerClick}
          position={{
            lat: 10.490106,
            lng: -66.875577
          }}
        />
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyAx5k23lQTdf3VXoquhaLeWPxF4xfl3Fho",
  LoadingContainer: Loading
})(MapContainer);
