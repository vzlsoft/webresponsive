import React, { Component } from "react";
import "./App.css";
import { getWorkbook, getSheet } from "sheetsy";

import Loading from "./Components/Loading";
import Search from "./Components/Search";
import Map from "./Components/Map";
import Input from "./Input";

import Logo from "./Assets/logo.png";
import back from "./Assets/back.svg";

import { Button } from "react-bootstrap";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const Home = () => (
  <div>
    <p className="App-intro" style={{ paddingTop: "15px" }}>
      Empieza buscando tu Activo
    </p>
    <Link to="/search">
      <Button bsStyle="primary" style={{ backgroundColor: "#222" }}>
        Buscar
      </Button>
    </Link>
  </div>
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      activos: [],
      pin: ""
    };
  }

  async componentDidMount() {
    const key = "1VqpWtQ5UcIZvR8w0bF3Vi6stGS3ivuTVVPvZf6Kh0kY";
    const book = await getWorkbook(key);
    const activosSheet = await getSheet(key, book.sheets[0].id);
    const pinSheet = await getSheet(key, book.sheets[1].id);

    this.setState({
      pin: pinSheet.rows[0].pin,
      activos: activosSheet.rows,
      loading: false
    });
  }

  render() {
    console.log(this.state);
    const { activos, pin, loading } = this.state;
    return loading ? (
      <Loading />
    ) : (
      <Router>
        <div className="App">
          <header className="App-header">
            {window.location.pathname === "/" ? null : <Back />}
            <img src={Logo} alt="" className="App-logo" />
          </header>
          <Route
            path="/pin"
            render={props => <Input {...props} grant={pin} />}
          />
          <Route exact path="/" component={Home} />
          <Route
            path="/search"
            render={props => <div><Search {...props} activos={activos} /><Search {...props} activos={activos} /></div>}
          />
          <Route path="/map" component={Map} />
        </div>
      </Router>
    );
  }
}

export default App;

const Back = props => (
  <Link to="/search">
    <img alt="" src={back} className="App-icon" />
  </Link>
);
